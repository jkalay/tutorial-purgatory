# Tutorial Purgatory

A site for aspiring web developers to find and share tutorials while on their development journey.

## Technologies

- Model-View-Controller architecture
- Node.js/Express
- MongoDB/Mongoose
- Passport
- EJS
- Bootstrap

## Planned additions

- Set up REST API with JSON representations of server-side data
- Introduce additional strategies (such as Google OAuth)
- Dev Chat
