const mongoose = require('mongoose');
const Tutorial = require('./models/Tutorial');
const User = require('./models/User');

mongoose
  .connect(
    process.env.MONGODB_URI || 'mongodb://localhost:27017/tutorial_purgatory',
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  )
  .then(() => console.log('Connected to MongoDB successfully!'))
  .catch(err => console.error('Could not connect to MongoDB', err));
