const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

userSchema = new mongoose.Schema(
  {
    name: {
      first: {
        type: String,
        trim: true,
        required: true
      },
      last: {
        type: String,
        trim: true,
        required: true
      }
    },
    email: {
      type: String,
      lowercase: true,
      unique: true,
      required: true
    },
    tutorials: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tutorial' }]
  },
  {
    timestamps: true
  }
);

userSchema.virtual('fullName').get(function() {
  return `${this.name.first} ${this.name.last}`;
});

userSchema.plugin(passportLocalMongoose, {
  usernameField: 'email'
});

module.exports = mongoose.model('User', userSchema);
