'use strict';

const mongoose = require('mongoose');

const tutorialSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true
  },
  instructor: {
    type: String
  },
  description: {
    type: String,
    required: true
  },
  paid: {
    type: Boolean,
    required: true
  },
  url: {
    type: String
  },
  author: {
    type: mongoose.Schema.Types.ObjectId
  }
});

module.exports = mongoose.model('Tutorial', tutorialSchema);
