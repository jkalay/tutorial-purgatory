'use strict';

const httpStatus = require('http-status-codes');

module.exports = {

  showNotFoundError: (req, res) => {
    res.status(httpStatus.NOT_FOUND);
    res.render('error');
  },

  internalServerError: (err, req, res, next) => {
    const errorCode = httpStatus.INTERNAL_SERVER_ERROR;
    console.log(`Error occurred: ${err.stack}`);
    res.status(errorCode);
    res.send(`${errorCode} | This application is experiencing a temporary hiccup!`);
  }

}