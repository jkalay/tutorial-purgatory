'use strict';

module.exports = {
  show: (req, res) => {
    res.render('index');
  },
  showAbout: (req, res) => {
    res.render('about');
  }
};
