const mongoose = require('mongoose');
const Tutorial = require('../models/Tutorial');
const User = require('../models/User');

module.exports = {
  find: (req, res, next) => {
    Tutorial.find()
      .then(tutorials => {
        res.locals.tutorials = tutorials;
        next();
      })
      .catch(err => {
        console.error(`Error finding Tutorials: ${err.message}`);
        next(err);
      });
  },

  redirect: (req, res) => {
    const path = res.locals.redirect;
    if (path) {
      res.redirect(path);
    } else {
      next();
    }
  },

  showAllTutorials: (req, res) => {
    res.render('tutorials/index');
  },

  new: (req, res) => {
    res.render('tutorials/new');
  },

  create: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    const params = {
      title: req.body.title,
      instructor: req.body.instructor,
      description: req.body.description,
      paid: req.body.paid === 'true' ? true : false,
      url: req.body.url,
      author: req.user._id
    };

    Tutorial.create(params)
      .then(tutorial => {
        req.flash('success', `${tutorial.title} entry created successfully!`);
        res.locals.tutorial = tutorial;
        res.locals.redirect = '/tutorials';
        next();
      })
      .catch(err => {
        req.flash('error', `Failed to create tutorial: ${err.message}`);
        res.locals.redirect = '/tutorials';
        console.log(`Error creating Tutorial: ${err.message}`);
        next();
      });
  },

  checkValidId: (req, res, next) => {
    const tutorialId = req.params.id;
    if (mongoose.Types.ObjectId.isValid(tutorialId)) {
      next();
    } else {
      res.render('error');
    }
  },

  findById: (req, res, next) => {
    const tutorialId = req.params.id;

    Tutorial.findById(tutorialId)
      .then(tutorial => {
        User.findById(tutorial.author)
          .then(user => {
            res.locals.tutorial = tutorial;
            res.locals.author = user;
            next();
          })
          .catch(err => {
            console.error(`Error finding User by User ID: ${err.message}`);
            next(err);
          });
      })
      .catch(err => {
        console.error(`Error finding Tutorial by Tutorial ID: ${err.message}`);
        next(err);
      });
  },

  showTutorial: (req, res) => {
    res.render('tutorials/show');
  },

  showEdit: (req, res) => {
    res.render('tutorials/edit');
  },

  update: (req, res, next) => {
    const tutorialId = req.params.id;
    const params = {
      title: req.body.title,
      instructor: req.body.instructor,
      description: req.body.description,
      paid: req.body.paid,
      url: req.body.url
    };

    Tutorial.findByIdAndUpdate(tutorialId, {
      $set: params
    })
      .then(tutorial => {
        req.flash(
          'success',
          `Entry for ${tutorial.title} updated successfully!`
        );
        res.locals.tutorial = tutorial;
        res.locals.redirect = `/tutorials/${tutorialId}`;
        next();
      })
      .catch(err => {
        req.flash('error', `Failed to update tutorial entry: ${err.message}`);
        res.locals.redirect = `/tutorials/${tutorialId}`;
        console.error(`Error updating Tutorial by Tutorial ID: ${err.message}`);
        next();
      });
  },

  delete: (req, res, next) => {
    const tutorialId = req.params.id;

    Tutorial.findByIdAndDelete(tutorialId)
      .then(tutorial => {
        req.flash(
          'success',
          `Entry for ${tutorial.title} was deleted successfully.`
        );
        res.locals.redirect = '/tutorials';
        next();
      })
      .catch(err => {
        req.flash('error', `Failed to delete tutorial entry: ${err.message}`);
        res.locals.redirect = '/tutorials';
        console.error(`Error deleting Tutorial by Tutorial ID: ${err.message}`);
        next();
      });
  },

  validate: (req, res, next) => {
    req.check('title', 'Title cannot be empty').notEmpty();
    req
      .check('title', 'Please provide a title between 3 and 50 characters')
      .isLength({ min: 3, max: 50 })
      .trim()
      .escape();
    req.check('instructor', 'Instructor name cannot be empty').notEmpty();
    req
      .check(
        'instructor',
        'Please provide an instructor name between 3 and 50 characters'
      )
      .isLength({ min: 3, max: 50 })
      .trim()
      .escape();
    req.check('url', 'URL cannot be empty').notEmpty();
    req
      .check('url', 'Please provide a URL between 7 and 100 characters')
      .isLength({ min: 7, max: 100 });
    req.check('url', 'Please provide a valid URL').isURL();
    req.check('description', 'Description cannot be empty').notEmpty();
    req
      .check(
        'paid',
        'Please select whether or not the tutorial requires payment'
      )
      .notEmpty();
    req
      .check(
        'description',
        'Please provide a description between 20 and 1000 characters'
      )
      .isLength({ min: 20, max: 1000 })
      .trim()
      .escape();

    req.getValidationResult().then(err => {
      if (!err.isEmpty()) {
        const messages = err.array().map(e => e.msg);
        req.flash('error', messages);
        req.skip = true;
        res.locals.redirect = '/tutorials/new';
        next();
      } else {
        next();
      }
    });
  },

  checkAuthenticated: (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    } else {
      req.flash('error', 'Please log in to perform this action.');
      res.redirect('/users/login');
    }
  }
};
