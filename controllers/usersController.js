const mongoose = require('mongoose');
const passport = require('passport');
const User = require('../models/User');

module.exports = {
  find: (req, res, next) => {
    User.find()
      .then(users => {
        res.locals.users = users;
        next();
      })
      .catch(err => {
        console.error(`Error finding Users: ${err.message}`);
        next(err);
      });
  },

  redirect: (req, res, next) => {
    const path = res.locals.redirect;
    if (path) {
      res.redirect(path);
    } else {
      next();
    }
  },

  showAllUsers: (req, res) => {
    res.render('users/index');
  },

  new: (req, res) => {
    res.render('users/new');
  },

  create: (req, res, next) => {
    if (req.skip) {
      return next();
    }

    const params = {
      name: {
        first: req.body.first,
        last: req.body.last
      },
      email: req.body.email
    };

    const newUser = new User(params);

    console.log(newUser);

    User.register(newUser, req.body.password, (err, user) => {
      if (user) {
        req.flash(
          'success',
          `${user.fullName}'s account created successfully! Please log in!`
        );
        res.locals.redirect = '/users';
        next();
      } else {
        req.flash('error', `Failed to create user account: ${err.message}`);
        res.locals.redirect = '/users/new';
        console.log(`Error creating User: ${err.message}`);
        next();
      }
    });
  },

  checkValidId: (req, res, next) => {
    const userId = req.params.id;
    if (mongoose.Types.ObjectId.isValid(userId)) {
      next();
    } else {
      res.render('error');
    }
  },

  findById: (req, res, next) => {
    const userId = req.params.id;

    User.findById(userId)
      .then(user => {
        res.locals.user = user;
        next();
      })
      .catch(err => {
        console.error(`Error finding User by User ID: ${err.message}`);
        next(err);
      });
  },

  showUser: (req, res) => {
    res.render('users/show');
  },

  login: (req, res) => {
    res.render('users/login');
  },

  logout: (req, res, next) => {
    req.logout();
    req.flash('success', 'You have logged out successfully.');
    res.locals.redirect = '/';
    next();
  },

  authenticate: passport.authenticate('local', {
    failureRedirect: '/users/login',
    failureFlash: 'Invalid credentials. Please try again.',
    successRedirect: '/',
    successFlash: 'You have been logged in successfully!'
  }),

  validate: (req, res, next) => {
    req.sanitizeBody('first').trim();
    req.sanitizeBody('last').trim();
    req
      .sanitizeBody('email')
      .normalizeEmail({
        all_lowercase: true
      })
      .trim();
    req.check('first', 'First name cannot be empty').notEmpty();
    req
      .check('first', 'First name must be between 2 and 30 characters')
      .isLength({ min: 2, max: 30 });
    req.check('last', 'Last name cannot be empty').notEmpty();
    req
      .check('last', 'Last name must be between 2 and 30 characters')
      .isLength({ min: 2, max: 30 });
    req.check('email', 'Email is invalid').isEmail();
    req.check('password', 'Password cannot be empty').notEmpty();
    req
      .check('password', 'Password must be between 5 and 20 characters')
      .isLength({ min: 5, max: 20 });

    req.getValidationResult().then(err => {
      if (!err.isEmpty()) {
        const messages = err.array().map(e => e.msg);
        req.flash('error', messages);
        req.skip = true;
        res.locals.redirect = '/users/new';
        next();
      } else {
        next();
      }
    });
  },

  checkAuthenticated: (req, res, next) => {
    if (req.isAuthenticated()) {
      return next();
    } else {
      req.flash('error', 'Please log in to perform this action.');
      res.redirect('/users/login');
    }
  }
};
