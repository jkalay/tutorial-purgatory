'use strict';

const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const connectFlash = require('connect-flash');
const expressSession = require('express-session');
const expressValidator = require('express-validator');
const layouts = require('express-ejs-layouts');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const passport = require('passport');
const routes = require('./routes');
const User = require('./models/User');

mongoose
  .connect(
    process.env.MONGODB_URI || 'mongodb://localhost:27017/tutorial_purgatory',
    {
      useCreateIndex: true,
      useFindAndModify: false,
      useNewUrlParser: true,
      useUnifiedTopology: true
    }
  )
  .then(() => console.log('Connected to MongoDB successfully!'))
  .catch(err => console.error('Could not connect to MongoDB', err));

app.set('port', process.env.PORT || 3000);
app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(methodOverride('_method', { methods: ['POST', 'GET'] }));
app.use(layouts);
app.use(express.static('public'));

app.use(cookieParser(process.env.COOKIE_KEY || 'top_secret'));
app.use(
  expressSession({
    secret: process.env.SESSION_SECRET || 'top_secret',
    cookie: {
      maxAge: 1814400000
    },
    resave: false,
    saveUninitialized: false
  })
);
app.use(passport.initialize());
app.use(passport.session());
passport.use(User.createStrategy());
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
app.use(connectFlash());
app.use((req, res, next) => {
  res.locals.flashMessages = req.flash();
  res.locals.loggedIn = req.isAuthenticated();
  res.locals.currentUser = req.user;
  next();
});
app.use(expressValidator());

app.use('/', routes);

app.listen(app.get('port'), () => {
  console.log(
    `Express server is listening on http://localhost:${app.get('port')}`
  );
});
