'use strict';

const router = require('express').Router();
const controller = require('../controllers/tutorialsController');

router.get('/', controller.find, controller.showAllTutorials);

router.get('/new', controller.checkAuthenticated, controller.new);

router.get(
  '/:id',
  controller.checkValidId,
  controller.findById,
  controller.showTutorial
);

router.get(
  '/edit/:id',
  controller.checkAuthenticated,
  controller.checkValidId,
  controller.findById,
  controller.showEdit
);

router.post(
  '/create',
  controller.checkAuthenticated,
  controller.validate,
  controller.create,
  controller.redirect
);

router.put(
  '/update/:id',
  controller.checkAuthenticated,
  controller.update,
  controller.redirect
);

router.delete(
  '/delete/:id',
  controller.checkAuthenticated,
  controller.delete,
  controller.redirect
);

module.exports = router;
