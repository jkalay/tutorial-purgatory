'use strict';

const router = require('express').Router();
const controller = require('../controllers/errorController');

router.use(controller.showNotFoundError);

module.exports = router;