const router = require('express').Router();
const controller = require('../controllers/usersController');

router.get('/', controller.find, controller.showAllUsers);

router.get('/new', controller.new);

router.get('/login', controller.login);

router.get('/logout', controller.logout, controller.redirect);

router.get(
  '/:id',
  controller.checkValidId,
  controller.findById,
  controller.showUser
);

router.post(
  '/create',
  controller.validate,
  controller.create,
  controller.redirect
);

router.post('/login', controller.authenticate);

module.exports = router;
