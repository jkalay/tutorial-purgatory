'use strict';

const router = require('express').Router();
const controller = require('../controllers/homeController');

router.get('/', controller.show);

router.get('/about', controller.showAbout);

module.exports = router;
