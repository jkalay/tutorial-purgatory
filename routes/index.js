'use strict';

const router = require('express').Router();
const tutorialRoutes = require('./tutorialRoutes');
const homeRoutes = require('./homeRoutes');
const errorRoutes = require('./errorRoutes');
const userRoutes = require('./userRoutes');

router.use('/tutorials', tutorialRoutes);
router.use('/users', userRoutes);
router.use('/', homeRoutes);
router.use('/', errorRoutes);

module.exports = router;